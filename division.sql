﻿DROP DATABASE IF EXISTS division;
CREATE DATABASE division;
USE division;

CREATE OR REPLACE TABLE autores (
  idautor int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar (20)
  );
CREATE OR REPLACE TABLE libros (
idlibro int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar (20)
);
CREATE OR REPLACE TABLE escritos (
id int AUTO_INCREMENT PRIMARY KEY,
  autor int,
  libro int,
  horas int,
  CONSTRAINT fkescriautor FOREIGN KEY (autor) REFERENCES autores (idautor),
  CONSTRAINT fkescrilibro FOREIGN KEY (libro) REFERENCES libros (idlibro)
);

INSERT INTO libros (nombre)
  VALUES ('libro1'),('libro2'),('libro3'),('libro4');
INSERT INTO autores (nombre)
  VALUES ('autor1'),('autor2'),('autor3');
INSERT INTO escritos (autor, libro, horas)
  VALUES (1,1,10),(1,2,20),(1,3,10),(2,1,10),(2,2,10);
INSERT INTO escritos ( autor, libro, horas)
  VALUES (1,NULL,0);

-- numerador
SELECT DISTINCT e.libro, e.autor FROM escritos e;
-- denomimador 
SELECT DISTINCT e.libro FROM escritos e; 

SELECT autor, COUNT(DISTINCT libro) nlibros FROM 
(SELECT DISTINCT e.libro, e.autor FROM escritos e) c1 
GROUP BY c1.autor HAVING nlibros=(SELECT COUNT(DISTINCT e.libro) FROM escritos e );