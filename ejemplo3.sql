﻿USE ejemplo3programacion;

DELIMITER //
CREATE OR REPLACE PROCEDURE alumnos(id1 int,id2 int)
  COMMENT 'iniciar un procedimiento almacenado que cuando le llames como argumentos: -id1 inicial -id2: id final actualice la notas minima,
  maxima, media, y moda de los alumnos (utilizando las funciones realizadas) que esten comprendidos entre los id pasados. ademas, el mismo 
  procedimiento debe actualizar la tabla grupos colocando la nota media de los alumnos que esten comprendidos entre los id pasados y por grupo' 
  BEGIN
  update alumnos
    SET 
    media=(nota1, nota2, nota3, nota4),
    min=( nota1, nota2, nota3, nota4),
    max=( nota1, nota2, nota3, nota4),
    moda=( nota1, nota2, nota3, nota4)
  WHERE
    id BETWEEN id1 AND id2;
  UPDATE grupos
    SET media=(SELECT AVG( media) FROM alumnos WHERE grupo=1 AND id BETWEEN id1 AND id2)
  WHERE id=1;
  UPDATE grupos
    SET media=( SELECT AVG(media) FROM alumnos WHERE grupo=2 AND id BETWEEN id1 AND id2)
  WHERE id=2;
  
  END //
DELIMITER ;

DELIMITER //
CREATE OR REPLACE PROCEDURE aretrian (base int,alt int)
  COMMENT' procedimiento que le das dos argumentos y te devuelve el area del triangulo'
  BEGIN
   DECLARE area int DEFAULT 0;
  SET area= base* alt/2;
  SELECT area;
  END //
DELIMITER ;
  CALL aretrian(5,7);

 DELIMITER //
 CREATE OR REPLACE FUNCTION areatriang (base int,alt int)
   RETURNS float
  COMMENT 'funcion que le das dos argumentos y te devuelve el area del triangulo'
   BEGIN
      DECLARE area int DEFAULT 0;
      SET area= base* alt/2;
      RETURN area;
   END//
 DELIMITER;

SELECT areatriang(10,2);

 DELIMITER //
 CREATE OR REPLACE FUNCTION perimetrotrian (base int,lado1 int,lado2 int)
   RETURNS float
  COMMENT 'funcion que le das tres argumentos y te devuelve el perimetro del triangulo'
   BEGIN
      DECLARE perim int  DEFAULT 0;
      SET perim=( base+ lado1+ lado2)  ;
      RETURN perim ;
   END//
 DELIMITER;

SELECT perimetrotrian(5, 7, 3) ;

DELIMITER //
CREATE OR REPLACE PROCEDURE areayperim (id1 int, id2 int)
  COMMENT 'le das dos argumentos y te actualiza el area y el perimetro de los triangulos 
  (utilizando las funciones realizadas anteriormente) que esten comprendidos entre los id pasados'
  BEGIN
  UPDATE triangulos
    SET area=areatriang(base,altura),perimetro=perimetrotrian(base,lado2,lado3)
    WHERE 
    id BETWEEN id1 AND id2;
        
  END //
DELIMITER ;

CALL areayperim(18,18);
SELECT * FROM triangulos;
UPDATE triangulos
  set area=NULL;

 DELIMITER //
 CREATE OR REPLACE FUNCTION areacuadrado( lado int)
   RETURNS float
  COMMENT 'funcion que recibe como argumentos un lado y devuelve el area del cuadrado'
   BEGIN
      DECLARE arec float  DEFAULT 0;
      SET arec= (lado* lado)  ;
      RETURN arec ;
   END//
 DELIMITER;

SELECT areacuadrado(8) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION pericuad( lado int)
   RETURNS float
  COMMENT ' funcion que recibe como argumentos un lado y devuelve el perimetro del cuadrado'
   BEGIN
      DECLARE perim float DEFAULT 0;
      SET perim= ( lado*4) ;
      RETURN perim ;
   END//
 DELIMITER;

SELECT pericuad(9) ;

DELIMITER //
CREATE OR REPLACE PROCEDURE areayperimcuad (id1 int,id2 int)
  COMMENT 'procedimiento que cuando llamas como argumentos "id1:id inicial y id2:id final 
  actualiza el area y el perimetro de los cuadrados utilizando las funciones anteriores
  que estan comprendidos entre los id pasados' 
  BEGIN
  UPDATE cuadrados
    set area= areacuadrado( lado),perimetro= pericuad(lado) WHERE id BETWEEN id1 AND id2 ;
   SELECT * FROM cuadrados;      
  END //
DELIMITER ;
CALL areayperimcuad(4,8);

 DELIMITER //
 CREATE OR REPLACE FUNCTION arearect(lado1 int,lado2 int)
   RETURNS float
  COMMENT 'funcion donde recibe dos argumentos lado1 y lado2 y devuelve el area del rectangulo'
   BEGIN
      DECLARE arear int  DEFAULT 0;
      SET arear= lado1* lado2;
      RETURN arear ;
   END//
 DELIMITER;

SELECT arearect(20,10) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION perirec(lado1 int, lado2 int)
   RETURNS float
  COMMENT 'recibe dos parametros y devuelve el perimetro del rectangulo'
   BEGIN
      DECLARE perim int  DEFAULT 0;
      SET perim= lado1+ lado2+ lado1+ lado2 ;
      RETURN perim ;
   END//
 DELIMITER;

SELECT perirec(19,34) ;

DELIMITER //
CREATE OR REPLACE PROCEDURE areayperect(id1 int,id2 int)
  COMMENT' recibe dos argumentos id1(id inicial),id2(id final) y devuelve el area y el perimetro
  de los rectangulos (utilizando las funciones anteriores) comprendidos entre los id pasados' 
  BEGIN
  UPDATE rectangulo
    SET area= arearect( lado1, lado2), perimetro= perirec( lado1, lado2) WHERE id BETWEEN id1 AND id2 ;
   SELECT * FROM rectangulo;    
  END //
DELIMITER ;
CALL areayperect (7,8);

 DELIMITER //
 CREATE OR REPLACE FUNCTION areacirc(radio int )
   RETURNS float
  COMMENT 'recibe un argumento llamado radio y devuelve el area del circulo' 
   BEGIN
      DECLARE area float DEFAULT 0;
      SET area=(PI()* POW( radio,2))  ;
      RETURN area ;
   END//
 DELIMITER;

SELECT areacirc(14) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION pericirc( radio int)
   RETURNS float
  COMMENT 'recibe un parametro y devuelve el perimetro del circulo'
   BEGIN
      DECLARE perim int  DEFAULT 0;
      SET perim=2*PI()* radio  ;
      RETURN perim ;
   END//
 DELIMITER;

SELECT pericirc(14) ;

DELIMITER //
CREATE OR REPLACE PROCEDURE arepecirc( id1 int,id2 int)
  COMMENT 'recibe dos argumentos y devuelve el area y perimetro del circulo anteriormente realizados en los id citados'
  BEGIN
  UPDATE circulo   
    SET area= areacirc( radio), perimetro= pericirc( radio) WHERE id BETWEEN id1 AND id2  ;
   END //
DELIMITER ;
SELECT * FROM circulo; 
CALL arepecirc(1,7);
-- esto limpia los campos area y perimetro de la tabla circulo --
UPDATE circulo 
  SET area=NULL, perimetro= NULL;

 DELIMITER //
 CREATE OR REPLACE FUNCTION mediad (d1 int, d2 int,d3 int, d4 int)
   RETURNS float
  COMMENT 'recibe cuatro argumentos y devuelve la media de ellos' 
   BEGIN
      DECLARE media float DEFAULT 0;
      SET media= (d1+ d2+ d3+ d4)/4  ;
      RETURN media ;
   END//
 DELIMITER;

SELECT mediad(5,8,7,9) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION minimod (d1 int, d2 int,d3 int, d4 int)
   RETURNS float 
  COMMENT 'recibe cuatro argumentos y devuelve el minimo de ellos'
   BEGIN
      DECLARE minimo float  DEFAULT 0;
      SET minimo= LEAST( d1, d2, d3, d4)  ;
      RETURN minimo ;
   END//
 DELIMITER;

SELECT minimod(4,5,6,7) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION maximd(d1 int, d2 int,d3 int, d4 int)
   RETURNS float
  COMMENT 'recibe cuatro argumentos y devuelve el mayor de ellos' 
   BEGIN
      DECLARE max int  DEFAULT 0;
      SET max=GREATEST( d1, d2, d3, d4)  ;
      RETURN max ;
   END//
 DELIMITER;

SELECT maximd(9,8,0,7) ;

 DELIMITER //
 CREATE OR REPLACE FUNCTION modad(d1 int, d2 int,d3 int, d4 int)
   RETURNS int
   BEGIN
      DECLARE moda int  DEFAULT 0;
  CREATE OR REPLACE TEMPORARY TABLE m (
    id int AUTO_INCREMENT,t1 int, PRIMARY KEY(id)
    );
  INSERT INTO m ( t1)VALUES (d1), (d2), (d3), (d4);
    SET moda= (SELECT t1 FROM m GROUP BY t1 ORDER BY COUNT(*)  DESC LIMIT 1);   
      RETURN moda ;
   END//
 DELIMITER;  
SELECT modad(5,6,7,8) ;


DELIMITER //
CREATE OR REPLACE PROCEDURE medmasminmod(id1 int, id2 int )
  COMMENT 'recibe dos argumentos y actualiza las notas minima,maxima,media y moda de las funciones realizadas anteriormente' 
  BEGIN
  UPDATE alumnos
    SET media=mediad( nota1,nota2,nota3,nota4), min= minimod( nota1,nota2,nota3,nota4), max= maximd(nota1, nota2, nota3, nota4),
 moda= modad( nota1,nota2,nota3,nota4) WHERE id BETWEEN id1 AND id2 ;
   SELECT * FROM alumnos;      
  END //
DELIMITER ;
CALL medmasminmod(5,8);
-- esta instruccion limpia los registros de la tabla alumnos media,max,min y moda --
UPDATE alumnos
  SET media=NULL, max= NULL, min=NULL, moda=NULL;
SELECT * FROM alumnos;

DELIMITER //
CREATE OR REPLACE PROCEDURE conversion(id1)
  BEGIN
   UPDATE conversion
    SET m= cm/100, km= cm/100000,pulgadas=cm/0.393701 WHERE cm IS NOT NULL AND id> id1;
   UPDATE conversion
  SET cm=m*100,km=m/1000,pulgadas=cm/0.393701 WHERE m IS NOT NULL AND id1> id;
   UPDATE conversion
  SET cm=km*100000, m=km*1000, pulgadas=cm/0.393701 WHERE km IS NOT NULL AND cm IS NULL AND id> id1; 
   UPDATE conversion
SET cm=pulgadas*2.54,m=pulgadas*254,km=pulgadas*254000 WHERE pulgadas IS NOT NULL AND cm IS NULL AND id> id1;
  END //
DELIMITER ;

