﻿USE dbdatos;
-- esto actualiza registros de las tablas
UPDATE datos
  SET suma= numero1+ numero2
WHERE suma IS NULL;
-- este es otro
UPDATE datos
  SET resta= numero1- numero2;


DELIMITER //
CREATE OR REPLACE PROCEDURE p16()
  COMMENT 'hace un bucle, metiendole en una tabla con while'
  BEGIN
   DECLARE b int DEFAULT 1;
CREATE OR REPLACE TEMPORARY TABLE tabla (
    n1 int
  );
  WHILE(b<=10) DO
 INSERT INTO tabla VALUES (b);  
    SET b= b+1;
      END WHILE;
  SELECT * FROM tabla;
 END //
DELIMITER ;
  CALL p16();

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p17(arg1 int)
    COMMENT 'bucle que le das un numero y que te muestra desde el numero 1 
    hasta el numero pasado utilizando una tabla temporal con la funcion while'
    BEGIN
     DECLARE n int DEFAULT 1;
    CREATE OR REPLACE TEMPORARY TABLE tabla(
    n1 int
    );
    WHILE(n< arg1) DO
    INSERT INTO tabla VALUE ( n);
    SET n= n+1;
    END WHILE;
    SELECT* FROM tabla;
    END //
  DELIMITER ;
CALL p17(45);

 DELIMITER //
  CREATE OR REPLACE PROCEDURE p17(arg1 int)
    COMMENT 'bucle que le das un numero y que te muestra desde el numero 1 
    hasta el numero pasado utilizando una tabla temporal con la funcion iterate'
    BEGIN
     DECLARE n int DEFAULT 1;
    CREATE OR REPLACE TEMPORARY TABLE tabla(
    n1 int
    );
etiqueta: WHILE(n< arg1) DO
      IF(n=1) THEN
     SET n= n+1;  
    ITERATE etiqueta;
    END IF;
    INSERT INTO tabla VALUE ( n);
  SET n=n+1;
  END WHILE etiqueta;
    SELECT* FROM tabla;
    END //
  DELIMITER ;
CALL p17(18);

DELIMITER //
DROP FUNCTION IF EXISTS f1 //
CREATE OR REPLACE FUNCTION f1(n1 int,n2 int)
  RETURNS int
  COMMENT 'le das dos numeros y devuelve la suma de ellos mediante una funcion' 
  BEGIN
DECLARE resultado int DEFAULT 0;
  SET resultado= n1+ n2;
  RETURN resultado;
  END//
DELIMITER;
 
SELECT f1(5,6);


