﻿CREATE DATABASE m1u3ejemplo1;
USE m1u3ejemplo1;

DELIMITER //
CREATE OR REPLACE PROCEDURE r1 (n1 int,n2 int)
  BEGIN
   IF n1> n2 THEN
    SELECT n1;
    ELSE
    SELECT n2;
    END IF;
  END //
DELIMITER ;
  CALL r1(6,6);
  CALL r1(3,4);

  DELIMITER //
  CREATE OR REPLACE PROCEDURE r1a (n1 int,n2 int)
    BEGIN
    DECLARE mayor int;
    SET mayor=GREATEST(n1,n2);
    SELECT mayor;
    END //
  DELIMITER ;
CALL r1a(7,8);

DELIMITER //
CREATE OR REPLACE PROCEDURE r1b(n1 int,n2 int)
  BEGIN
  CREATE TEMPORARY TABLE numero(
  n int
  );
  INSERT INTO numero VALUES ( n1), (n2);
  SELECT MAX(n) FROM numero;
  END //
DELIMITER ;
CALL r1b(7,5);

DELIMITER //
CREATE OR REPLACE PROCEDURE r2 (n1 int,n2 int,n3 int)
  BEGIN
   IF n1>n2 THEN
    IF n1> n3 THEN 
      SELECT n1;
    ELSE
      SELECT n3;
    END IF; 
  ELSE
    IF n2> n3 THEN
      SELECT n2;
    ELSE 
      SELECT n3;
    END IF;
   END IF;
  END //
DELIMITER ;
CALL r2(44,55,33);

DELIMITER //
CREATE OR REPLACE PROCEDURE r2 (n1 int,n2 int,n3 int)
  BEGIN
   DECLARE mayor int;
  SET mayor=GREATEST(n1, n2, n3);
  SELECT mayor;
  END //
DELIMITER ;
CALL r2(65,87,32);

DELIMITER //
CREATE OR REPLACE PROCEDURE r2 (n1 int,n2 int,n3 int)
  BEGIN
   CREATE TEMPORARY TABLE IF NOT EXISTS t(
  numero int
  );
  INSERT INTO t VALUES (n1), (n2),(n3);
  SELECT MAX( numero) FROM t;
  END //
DELIMITER ;
CALL r2 (67,56,45);

DELIMITER //
CREATE OR REPLACE PROCEDURE r3 (n1 int,n2 int,n3 int, OUT grande int, OUT pequeño int)
  BEGIN
   SET grande=GREATEST(n1, n2, n3);
  SET pequeño=LEAST(n1, n2, n3); 
  END //
DELIMITER ;
CALL r3(5,6,7,@g,@p);
SELECT@g,@p;

DELIMITER //
CREATE OR REPLACE PROCEDURE d (fecha1 date, fecha2 date)
  BEGIN
   SELECT DATEDIFF( fecha1, fecha2);
  END //
DELIMITER ;
CALL d ("2020/12/03","2034/11/10");

DELIMITER //
CREATE OR REPLACE PROCEDURE tt (f1 date,f2 date, OUT dias int, OUT meses int, OUT anyos int)
  BEGIN
   SET dias= TIMESTAMPDIFF(DAY, f1,f2);
    SET meses=TIMESTAMPDIFF( MONTH, f1,f2);
      SET anyos=TIMESTAMPDIFF( year,  f1,f2); 
  END //
DELIMITER ;
CALL tt ("2039/12/20","2050/10/22",@dias,@meses,@anyos);  
