﻿USE provincias;


-- hacer procedimiento que reciba dos numeros y te indique el mayor
DROP PROCEDURE IF EXISTS nombre;
CREATE PROCEDURE nombre(n1 int,n2 int,n3 int)
  BEGIN
    IF n1>n2 THEN
      IF n1>n3 THEN
        SELECT n1;
      ELSE
        SELECT n3;
      END IF;
    ELSE
       IF n2> n3 THEN
        SELECT n2;
        ELSE 
        SELECT n3;
      END IF;
    END IF;
  END;

CALL nombre(3,4,6);

DROP PROCEDURE IF EXISTS ej1v2;
CREATE PROCEDURE ej1v2(a int,b int, c int)
  BEGIN 
-- para crear una tabla temporalse pone
CREATE TEMPORARY TABLE IF NOT EXISTS numeros (
 numero int
  );
  INSERT INTO numeros (numero) VALUES (a),(b),(c);
  SELECT MAX(numero) FROM numeros; 
DROP TABLE numeros;
  END;
CALL ej1v2(12,55,65);

DROP PROCEDURE IF EXISTS ej1v3;
  CREATE OR REPLACE PROCEDURE ej1v3(a int,b int,c int)
BEGIN
  SELECT GREATEST(a,b,c);
    END;
  CALL ej1v3(81,76,98);

  DROP PROCEDURE IF EXISTS tres;
  CREATE OR REPLACE PROCEDURE tres(a int,b int,c int,OUT d int,OUT e int)
    BEGIN
            
    END;

  DELIMITER //
  CREATE OR REPLACE PROCEDURE p7(base int, exponente int)
    COMMENT 'calcular la potencia de dos numeros'
    BEGIN
     DECLARE resultado int DEFAULT 0;
    SET resultado=POW(base,exponente);
    CREATE TABLE IF NOT EXISTS potencia(
    id int AUTO_INCREMENT,
      b int,
      e int,
      r int,
       PRIMARY KEY ( id)
    );
    INSERT INTO potencia(b,e,r) VALUES (base,exponente,resultado);
    END //
  DELIMITER ;
 CALL p7(3,3);
SELECT * FROM potencia; 

DELIMITER //
CREATE OR REPLACE PROCEDURE p8(numero int)
  COMMENT'calcular la raiz cuadrada de dos numeros'
  BEGIN
   DECLARE resultado float DEFAULT 0;
  SET resultado=SQRT( numero);
  CREATE TABLE IF NOT EXISTS raizcuadrada(
  id int AUTO_INCREMENT,
    a int,
    raiz float,
    PRIMARY KEY( id)
  );
 INSERT INTO raizcuadrada(a,raiz) VALUES( numero, resultado);
END //
DELIMITER ;
CALL p8(567);
SELECT a, raiz FROM raizcuadrada;
 
/* analizar el procedimiento y añadir lo necesario para que calcule la longitud del texto y 
lo almacene en la tabla en un campo llamado longitud*/
DELIMITER //
DROP PROCEDURE IF EXISTS p9 //
CREATE OR REPLACE PROCEDURE p9(argumento varchar(50))
  COMMENT 'al poner create or replace procedure es como si pones drop procedure if exists y create procedure a la vez'
  BEGIN
  DECLARE longitudtexto int DEFAULT 0;
  SET longitudtexto=CHAR_LENGTH( argumento);
  CREATE TABLE IF NOT EXISTS texto(
  id int AUTO_INCREMENT PRIMARY KEY,
    texto varchar(50),
    longitud int
  );
   INSERT INTO texto(texto, longitud) VALUE (argumento, longitudtexto);
  END //
DELIMITER ;
CALL p9('ejemplos varios');
SELECT * FROM texto;

/* la sintaxis basica del if*/ 
  IF(condicion) THEN
    -- sentencias verdadero
    ELSE ;
    -- sentencias falso
    END IF;
-- ejemplo
  DELIMITER //
  CREATE OR REPLACE PROCEDURE n (a int)
    BEGIN
     IF (a>100) THEN
        SELECT "el numero es mayor que 100";
       ELSE SELECT  "el numero es menor que 100";
      END IF;
    END //
  DELIMITER ;
    CALL n(353);
    CALL n(95);

    DELIMITER //
    CREATE OR REPLACE PROCEDURE n (a int)
      COMMENT 'le pasas un numero y me indica si es mayor, menor o igual'
      BEGIN
       IF(a>100) THEN
        SELECT 'es mayor que 100';
        ELSEIF (a=100) THEN
        SELECT 'es igual a 100';
        ELSEIF (a<100) THEN
        SELECT 'es menor que 100';
        END IF;
         END //
    DELIMITER ;
CALL n(120);
CALL n(100);
CALL n(90);

DELIMITER //
CREATE OR REPLACE PROCEDURE n(a int)
  COMMENT 'igual que el de arriba pero mejor codificado'
  BEGIN
   DECLARE resultado varchar(50) DEFAULT "igual a 100";
   IF(a>100) THEN
        SET resultado= "es mayor que 100";
        ELSEIF (a<100) THEN
        SET resultado="es menor que 100";
        END IF;
  SELECT resultado;
  END //
DELIMITER ;
CALL n(10);