﻿DROP DATABASE IF EXISTS dbdatos;
CREATE DATABASE IF NOT EXISTS dbdatos;
USE dbdatos;
CREATE TABLE IF NOT EXISTS datos(
datos_id int(11) NOT NULL AUTO_INCREMENT,
numero1 int(11) NOT NULL,
numero2 int(11) NOT NULL,
suma varchar(255) DEFAULT NULL,
resta varchar(255) DEFAULT NULL,
rango varchar(5) DEFAULT NULL,
texto1 varchar(25) DEFAULT NULL,
texto2 varchar(25) DEFAULT NULL,
junto  varchar(255) DEFAULT NULL,
logitud varchar(255) DEFAULT NULL,
tipo int(11) NOT NULL,
numero int(11) NOT NULL,
PRIMARY KEY ( datos_id)
);


DELIMITER //
DROP PROCEDURE IF EXISTS e1a //
CREATE OR REPLACE PROCEDURE e1a( IN texto varchar(200),IN caracter char(1))
  COMMENT 'procedimiento almacenado que reciba un texto y un caracter 
  debe indicarse si ese carácter está dentro del texto con la opcion locate'
  BEGIN 
  DECLARE resultado varchar(30) DEFAULT 'El caracter no esta';
  IF(LOCATE(caracter,texto)<>0) THEN 
  SET resultado='El caracter está';
    END IF;
  SELECT resultado;
  END //
DELIMITER ;
CALL e1a('ojaefj','o');

DELIMITER //
DROP PROCEDURE IF EXISTS e1b //
CREATE OR REPLACE PROCEDURE e1b(IN texto varchar(50),IN caracter char(1))
  COMMENT 'recibe un texto y un caracter indicando si ese caracter esta en el texto con la opcion position'
  BEGIN
  DECLARE resultado varchar(30) DEFAULT 'El caracter no esta';
   IF(POSITION(caracter IN texto)<>0) THEN
    SET resultado='El caracter está';
    END IF;
  SELECT resultado;
  END //
DELIMITER ;
CALL e1b('estoesuntexto','g');

DELIMITER //
DROP PROCEDURE IF EXISTS e2 //
CREATE OR REPLACE PROCEDURE e2(IN texto varchar(200),IN caracter varchar(1))
  COMMENT 'recibe un texto y un caracter y debe indicar todo el teto que haya, antes de que aparezca ese caracter'
  BEGIN
 DECLARE resultado varchar(30) DEFAULT NULL;
  SET resultado=SUBSTRING_INDEX( texto, caracter, 1);
  SELECT resultado;
  END //
DELIMITER ;
CALL e2('este es un texto de prueba','k');

DELIMITER //
DROP PROCEDURE IF EXISTS e3 //
CREATE OR REPLACE PROCEDURE e3(IN a int,IN b int,IN c int,OUT mayor int,OUT menor int)
  COMMENT 'recibe tres numeros y dos argumentos de tipo salida y devuelve el numero mas grande y el mas pequeño de los tres con la funcion if'
  BEGIN
   SET mayor=IF(a>b,IF(a>c,a,c),IF(b>c,b,c));
  SET menor=IF(a<b,IF(a<c,a,c),IF(b<c,b,c));
  SELECT mayor, menor; 
  END //
DELIMITER ;
CALL e3(43,54,76,@mayor,@menor);

DELIMITER //
DROP PROCEDURE IF EXISTS e4 //
CREATE OR REPLACE PROCEDURE e4()
  COMMENT 'muestra cuantos numeros1 y cuantos numeros2 son mayores que 50'
  BEGIN
 DECLARE resultado int;
  DECLARE n1 int;
  DECLARE n2 int;
  SELECT COUNT(*)INTO n1 FROM datos d WHERE d.numero1>50;
  SET n2=(SELECT COUNT(*) FROM datos d WHERE d.numero2>50);
  SET resultado= n1+n2;
  SELECT resultado;   
  END //
DELIMITER ;
CALL e4();

DELIMITER //
DROP PROCEDURE IF EXISTS e5 //
CREATE OR REPLACE PROCEDURE e5()
  COMMENT 'calcula la suma y resta de numero1 y numero2 de la tabla datos'
  BEGIN
   UPDATE datos d
    SET d.suma= d.numero1+ d.numero2;
  UPDATE datos d
    SET d.resta= d.numero1- d.numero2;
  END //
DELIMITER ;
CALL e5();

DELIMITER //
DROP PROCEDURE IF EXISTS e6 //
CREATE OR REPLACE PROCEDURE e6()
  COMMENT 'primero pone todos los valores de suma y resta a null y despues calcula
  la suma, solamente si el numero1 es mayor que el numero2 y calcula la resta del
  numero2-numero1 si el numero2 es mayor; o numero1-numero2 si el numero1 es mayor'
  BEGIN
 UPDATE datos d SET d.suma=NULL, d.resta=NULL;
  UPDATE datos d SET d.suma= d.numero1+ d.numero2
  WHERE d.numero1>= d.numero2;
  UPDATE datos d SET d.resta= d.numero1- d.numero2
  WHERE d.numero1> d.numero2;
  UPDATE datos d SET d.resta= d.numero2- d.numero1
  WHERE d.numero1<= d.numero2;  
  END //
DELIMITER ;
CALL e6();

DELIMITER //
DROP PROCEDURE IF EXISTS e7 //
CREATE OR REPLACE PROCEDURE e7()
  COMMENT 'coloca en el campo "junto" el texto1 y el texto2'
  BEGIN
UPDATE datos d SET d.junto= CONCAT( d.texto1," ", d.texto2);
  END //
DELIMITER ;
CALL e7();

DELIMITER //
DROP PROCEDURE IF EXISTS e8 //
CREATE OR REPLACE PROCEDURE e8()
  COMMENT 'coloca en campo "junto" el valor null; despues coloca en el mismo campo 
  el texto1-texto2 si el rango es A. Si el rango es B, coloca texto1+texto2;
  y si el rango es distinto coloca el texto1 solamente'
  BEGIN
 UPDATE datos d SET d.junto=NULL;
  UPDATE datos d SET d.texto1=TRIM( d.texto1), d.texto2=TRIM( d.texto2);
  UPDATE datos d SET d.junto=IF( d.rango='A', CONCAT( d.texto1,"-", d.texto2),IF( d.rango='b', CONCAT( d.texto1,'+', d.texto2), d.texto1));
  END //
DELIMITER ;
CALL e8();
