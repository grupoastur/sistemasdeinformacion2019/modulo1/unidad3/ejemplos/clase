﻿USE programacion;
DELIMITER //
DROP PROCEDURE IF EXISTS pej1 //
CREATE OR REPLACE PROCEDURE pej1(n1 int,n2 int)
  COMMENT 'realizar procedimiento almacenado que reciba dos números y te indique el mayor de ellos. con if'
  BEGIN
  -- definir las variables e iniciarlas
  IF (n1 > n2)
    THEN SELECT n1;
    ELSE SELECT n2;
    END IF;
  END //
DELIMITER ;
CALL pej1(87,84);

DELIMITER //
DROP PROCEDURE IF EXISTS pej2 //
CREATE OR REPLACE PROCEDURE pej2 (IN n1 int,IN n2 int)
  COMMENT 'igual que el anterior pero mejorando el algoritmo'
  BEGIN
  -- definimos variables y las iniciamos
  DECLARE s int DEFAULT 0;
  IF (n1> n2)
    THEN
    SET s= n1;
    ELSE SET s= n2;
    END IF;
    SELECT s;
  END //
DELIMITER ;
CALL pej2(45,87);

DELIMITER //
DROP PROCEDURE IF EXISTS p2//
CREATE OR REPLACE PROCEDURE p2(IN a int,IN b int)
  COMMENT 'igual que el anterior pero creando una tabla, insertando los datos dentro y haciendo la consulta'
  BEGIN
  CREATE OR REPLACE TABLE pri(
    id int AUTO_INCREMENT,
    num int,
    PRIMARY KEY(id));
  INSERT INTO pri (num) VALUES
    (a),(b);
 SELECT MAX(num) FROM pri p;
  END //
DELIMITER ;
CALL p2(124,198);

DELIMITER //
DROP PROCEDURE IF EXISTS p3 //
CREATE OR REPLACE PROCEDURE p3(IN n1 int, IN n2 int, IN n3 int)
  COMMENT 'igual que los anteriores pero con tres numeros'
  BEGIN
  IF (n1 > n2)
   THEN IF (n1> n3)
   THEN SELECT n1;
  ELSE SELECT n3;
  END IF;
  ELSEIF (n2> n3)
    THEN SELECT n2;
    ELSE SELECT n3;
  END IF;
  END //
DELIMITER ;
CALL p3(165,98,132);

DELIMITER //
DROP PROCEDURE IF EXISTS p4 //
CREATE OR REPLACE PROCEDURE p4(IN a int,IN b int,IN c int)
  COMMENT 'igual que el anterior pero mejorando el algoritmo'
  BEGIN
  DECLARE mayor int DEFAULT 0;
  IF ( a>b)
    THEN IF (a> c)
THEN SET mayor=a;
ELSE SET mayor=c;
END IF;
    ELSEIF (b>c)
    THEN SET mayor=b;
    ELSE SET mayor=c;
    END IF;
  SELECT mayor;  
  END //
DELIMITER ;
CALL p4(96,93,95);

DELIMITER //
DROP PROCEDURE IF EXISTS p5 //
CREATE OR REPLACE PROCEDURE p5(IN a int,IN b int,IN c int)
 COMMENT 'igual que el anterior creando una tabla, metiendo los parametros y haciendo la consulta'
  BEGIN
  DROP TABLE IF EXISTS seg;
  CREATE TABLE IF NOT EXISTS seg(
  id int AUTO_INCREMENT,
    n1 int,
    PRIMARY KEY (id)
  );
  INSERT INTO seg VALUES
(DEFAULT, a),
(DEFAULT, b),
(DEFAULT, c);
  SELECT s.id, MAX( s.n1) FROM seg s;  
  END //
DELIMITER ;
CALL p5(87,79,43);

DELIMITER //
DROP PROCEDURE IF EXISTS p6 //
CREATE OR REPLACE PROCEDURE p6(IN a int,IN b int,IN c int)
  BEGIN
  SELECT GREATEST( a, b, c);
  END //
DELIMITER ;
CALL p6(76,94,25);

DELIMITER //
DROP PROCEDURE IF EXISTS p9 //
CREATE OR REPLACE PROCEDURE p9(IN a int,IN b int,IN c int, OUT mayor int, OUT menor int)
  COMMENT 'recibe tres numeros y dos argumentos de tipo salida donde devuelve el numero mas grande y 
  el mas pequeño de los tres'
  BEGIN 
  SET mayor=GREATEST( a, b, c);
  SET menor=LEAST( a, b, c);
  SELECT menor, mayor;
  END //
DELIMITER ;
CALL p9(165,48,99,@,@);


DELIMITER //
DROP PROCEDURE IF EXISTS p7 //
CREATE OR REPLACE PROCEDURE p7( IN a date, IN b date)
  COMMENT 'recibe dos fechas y te muestra el numero de dias'
  BEGIN  
  SELECT DATEDIFF(a, b);
  END //
DELIMITER ;
CALL p7('1988/11/9','1998/12/22');

DELIMITER //
DROP PROCEDURE IF EXISTS p8 //
CREATE OR REPLACE PROCEDURE p8(IN a date,IN b date)
  COMMENT 'recibe dos fechas y te muestra el numero de meses'
  BEGIN
  SELECT TIMESTAMPDIFF( MONTH, a, b);  
  END //
DELIMITER ;
CALL p8('1988/05/9','1988/12/22');

DELIMITER //
DROP PROCEDURE IF EXISTS p10//
CREATE OR REPLACE PROCEDURE p10(IN fecha1 date,IN fecha2 date,OUT días int,OUT meses int,OUT anyos int)
  COMMENT 'recibe dos fechas y te devuelve en tres argumentos de salida
  los días, meses y años entre las dos fechas'
  BEGIN
  SET días=TIMESTAMPDIFF(DAY, fecha1, fecha2);
  SET meses=TIMESTAMPDIFF(MONTH, fecha1, fecha2);
  SET anyos=TIMESTAMPDIFF(YEAR, fecha1, fecha2);
  SELECT días, meses, anyos;  
  END //
DELIMITER ;
CALL p10('1988/6/1','1990/7/2',@días,@meses,@anyos);

DELIMITER //
DROP PROCEDURE IF EXISTS p11 //
CREATE OR REPLACE PROCEDURE p11(IN frase varchar(150))
  COMMENT 'recibe una frase y te devuelve el numero de caracteres'
  BEGIN  
  SELECT LENGTH(frase);
  END //
DELIMITER ;
CALL p11('esto es una frase');

DELIMITER //
CREATE OR REPLACE PROCEDURE p12(n int)
  COMMENT 'te pasa una nota y te devuelve: suspenso si la nota es menor que 5,
  suficiente si la nota es entre 5y7, notable si la nota esta entre 7y9 y sobresaliente 
  si la nota es mayor que 9'
  BEGIN
   DECLARE r varchar(50) DEFAULT "suspenso";
  IF (n>5 AND n<7) THEN
    SET r="suficiente";
    ELSEIF (n>=7 AND n<9) THEN
    SET r="notable";
    ELSEIF (n>=9) THEN
    SET r="sobresaliente";
    END IF;
  SELECT r;
  END //
DELIMITER ;
CALL p12 (9);

-- el mismo de arriba modificado para case
  DELIMITER //
CREATE OR REPLACE PROCEDURE p13(n int)
  COMMENT 'te pasa una nota y te devuelve: suspenso si la nota es menor que 5,
  suficiente si la nota es entre 5y7, notable si la nota esta entre 7y9 y sobresaliente 
  si la nota es mayor que 9'
  BEGIN
   DECLARE r varchar(50);
 CASE
  WHEN (n>=5 AND n<7) THEN
      SET r="suficiente";
    WHEN (n>=7 AND n<9) THEN
      SET r="notable";
    WHEN (n>=9) THEN
      SET r="sobresaliente";
    ELSE 
      SET r="suspenso";
   END CASE;
  SELECT r;
  END //
DELIMITER ;
CALL p13(5);

DELIMITER //
CREATE OR REPLACE PROCEDURE p14(n1 int, n2 int, OUT resultado int)
  COMMENT 'calcula la suma de dos numeros, la suma la debe almacenar en un argumento de salida llamada resultado'
  BEGIN
   SET resultado= n1+ n2;
  END //
DELIMITER ;
SET @suma=3; 
CALL p14(2,5,@suma);
SELECT @suma;

DELIMITER //
CREATE OR REPLACE PROCEDURE p15(n1 int,n2 int, INOUT resultado int, texto varchar(50))
  COMMENT 'recibe como argumentos: n1:nmero (argumento de entrada)
  n2:numero (argumento de entrada)
  resultado: numero (argumento de entrada o entrada o salida)
  calculo: texto (argumento de entrada)
  si calculo vale suma entonces resultado tendra la suma de n1 y n2
  si calculo vale producto entonces resultado tendra el producto de n1*n2'
  BEGIN
   IF texto="suma" THEN
    SET resultado=n1+n2;
    ELSEIF texto="producto" THEN
    SET resultado=n1*n2;
    END IF;
  END //
DELIMITER ;
CALL p15 (7,5,@r,"suma");
SELECT @r;

DELIMITER //
CREATE OR REPLACE PROCEDURE p16()
  BEGIN
   DECLARE b int DEFAULT 1;

  CREATE OR REPLACE TEMPORARY TABLE tabla (
    n1 int
  );
  WHILE(b<=10) DO
 INSERT INTO tabla VALUES (b);  
    SET b= b+1;
      END WHILE;
  SELECT * FROM tabla;
 END //
DELIMITER ;
  CALL p16();


 


