﻿DROP DATABASE IF EXISTS ejemplo6;
CREATE DATABASE ejemplo6;
USE ejemplo6;

CREATE OR REPLACE TABLE ventas (
  id int(11) NOT NULL AUTO_INCREMENT,
  producto varchar(5) DEFAULT NULL,
  precio int(11) NOT NULL,
  unidades int(11) NOT NULL,
  total int(11) NOT NULL,
  PRIMARY KEY (id)
);

INSERT INTO ventas VALUES
(1, 'p1', 10, 5, 50),
(2, 'p2', 20, 3, 60),
(3, 'p1', 10, 7, 70),
(4, 'p3', 30, 7, 210),
(5, 'p1', 10, 8, 80),
(6, 'p1', 10, 9, 90),
(7, 'p2', 20, 10, 200),
(8, 'p1', 10, 11, 110),
(9, 'p4', 30, 12, 360),
(10, 'p1', 10, 13, 130),
(11, 'p2', 20, 14, 280),
(12, 'p3', 30, 15, 450),
(13, 'p4', 30, 16, 480),
(14, 'p5', 30, 17, 510);

CREATE OR REPLACE TABLE productos(
  producto varchar(5),
  cantidad int DEFAULT 0,
  PRIMARY KEY(producto)
  );

INSERT INTO productos (producto)
  VALUES ('p1'),('p2'),('p3'),('p4'),('p5');
