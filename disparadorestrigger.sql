﻿USE programacion ;

CREATE OR REPLACE TABLE salas(
id int AUTO_INCREMENT PRIMARY KEY,
  butacas int,
  fecha date,
  edad int DEFAULT 0
);

CREATE OR REPLACE TABLE ventas(
id int AUTO_INCREMENT PRIMARY KEY,
  sala int,
  numero int DEFAULT 0
);



-- creamos un disparador
DELIMITER@@
CREATE OR REPLACE TRIGGER disparador1
  AFTER INSERT ON salas FOR EACH ROW;

  -- disparador de sala que me calcule la edad de la sala 
  -- en funcion de su fecha de alta CON INSERT para introducir datos

DELIMITER@@
CREATE OR REPLACE TRIGGER salasBi
  BEFORE INSERT ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(MONTH,NEW.FECHA,NOW());
 END@@
DELIMITER;

INSERT INTO salas(butacas, fecha)
  VALUES (50,"2018/1/1");
SELECT * FROM salas;

-- lo mismo que el anterior pero con update para actualizar 

DELIMITER@@
CREATE OR REPLACE TRIGGER salasBU
  BEFORE UPDATE ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
  
 END@@
DELIMITER;


  UPDATE salas
    SET fecha="2002/1/1";
    
SELECT * FROM salas;

-- ejemplo: necesito que la tabla salas disponga de tres campos nuevos
-- esos campos seran dia,mes,año
-- quiero que esos tres campos automaticamente tengan el dia,mes y año de la fecha
-- necesito un disparador para insertar y otro para actualizar

-- haciendo los campos nuevos en la tabla  
  ALTER TABLE salas
  ADD COLUMN dia int,
  ADD COLUMN mes int,
  ADD COLUMN anyo int;

-- esto no es lo que se pide pero lo hago para que me halle la diferencia de la fecha
-- este es para insertar
DELIMITER@@
CREATE OR REPLACE TRIGGER salasBi
  BEFORE INSERT ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
  SET new.dia=TIMESTAMPDIFF(DAY,NEW.FECHA,NOW());
  SET NEW.mes=TIMESTAMPDIFF(MONTH,NEW.FECHA,NOW());
  SET NEW.anyo=TIMESTAMPDIFF(year,NEW.FECHA,NOW());   
 END@@
DELIMITER;

-- este es para actualizar
DELIMITER@@
CREATE OR REPLACE TRIGGER salasBi
  BEFORE UPDATE ON salas 
  FOR EACH ROW
  BEGIN
  SET NEW.edad=TIMESTAMPDIFF(year,NEW.FECHA,NOW());
  SET new.dia=TIMESTAMPDIFF(DAY,NEW.FECHA,NOW());
  SET NEW.mes=TIMESTAMPDIFF(MONTH,NEW.FECHA,NOW());
  SET NEW.anyo=TIMESTAMPDIFF(year,NEW.FECHA,NOW());   
 END@@
DELIMITER;

INSERT INTO salas(butacas, fecha)
  VALUES (50,"2015/12/10");
SELECT * FROM salas;
DELETE FROM salas;


-- aqui se crea otra tabla
CREATE OR REPLACE TABLE ventas1(
  id int AUTO_INCREMENT PRIMARY KEY,
  fecha date,
  precioUnitario float,
  unidad int,
  precioFinal float
  );

-- aqui se inserta con un disparador haciendo una operacion
DELIMITER@@
CREATE OR REPLACE TRIGGER ventas1bi 
  BEFORE INSERT ON ventas1 
  FOR EACH ROW
  BEGIN
  SET NEW.precioFinal=NEW.precioUnitario*new.unidad;
 END@@
DELIMITER;

-- aqui se actualiza con un disparador haciendo la misma operacion
DELIMITER@@
CREATE OR REPLACE TRIGGER ventas1bu 
  BEFORE UPDATE ON ventas1 
  FOR EACH ROW
  BEGIN
  SET NEW.precioFinal=NEW.precioUnitario*new.unidad;
 END@@
DELIMITER;
 
-- aqui se hacen diferentes llamadas de insercion y actualizacion
INSERT INTO ventas1 ( fecha, precioUnitario, unidad) 
  VALUES ("2012/01/01", 5, 8);
SELECT * FROM ventas1; 
UPDATE ventas1 SET precioUnitario=10 WHERE id=1;
SELECT * FROM ventas1;
INSERT INTO  ventas1 SET id=5;

-- 1. Crear un disparador para la tabla ventas para que cuando metas un registro nuevo te
-- calcule el total automáticamente.
  
  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBI
    BEFORE INSERT
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p6', 15, 20);

  SELECT * FROM ventas v;

-- 2. Crear un disparador para la tabla ventas para que cuando inserte un registro me sume
-- el total a la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAI
    AFTER INSERT
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + NEW.total 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  INSERT INTO ventas (producto, precio, unidades)
    VALUES ('p3', 50, 10);

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 3. Crear un disparador para la tabla ventas para que cuando actualices un registro nuevo
-- te calcule el total automáticamente.

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 100
    WHERE v.id = 8;

  SELECT * FROM ventas v;

-- 4. Crear un disparador para la tabla ventas para que cuando actualice un registro me
-- sume el total a la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAU
    AFTER UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos p 
        SET p.cantidad = p.cantidad + (NEW.total - OLD.total) 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 50
    WHERE v.id = 8;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;


-- 5. Crear un disparador para la tabla productos que si cambia el código del producto te
-- sume todos los totales de ese producto de la tabla ventas

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosBU
    BEFORE UPDATE
    ON productos
    FOR EACH ROW
    BEGIN
      SET NEW.cantidad = (
        SELECT SUM(v.total) FROM ventas v
          WHERE v.producto = NEW.producto);
    END //
  DELIMITER ;
  
  -- Comprobamos su funcionamiento
  UPDATE productos p 
    SET p.producto = 'p6'
    WHERE p.producto = 'p4';

  SELECT * FROM productos p;

-- 6. Crear un disparador para la tabla productos que si eliminas un producto te elimine
-- todos los productos del mismo código en la tabla ventas

  DELIMITER //
  CREATE OR REPLACE TRIGGER productosAD
    AFTER DELETE
    ON productos
    FOR EACH ROW
    BEGIN
      DELETE FROM ventas
        WHERE OLD.producto = producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  DELETE FROM productos 
    WHERE producto = 'p2';

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 7. Crear un disparador para la tabla ventas que si eliminas un registro te reste el total del
-- campo cantidad de la tabla productos (en el campo cantidad).

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasAD
    AFTER DELETE
    ON ventas
    FOR EACH ROW
    BEGIN
      UPDATE productos  
        SET cantidad = cantidad - OLD.total 
        WHERE producto = OLD.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  DELETE FROM ventas 
    WHERE id = 5;

  DELETE FROM ventas 
    WHERE id = 14;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

-- 8. Modificar el disparador 3 para que modifique la tabla productos actualizando el valor
-- del campo cantidad en funcion del total.

  DELIMITER //
  CREATE OR REPLACE TRIGGER ventasBU
    BEFORE UPDATE
    ON ventas
    FOR EACH ROW
    BEGIN
      SET NEW.total = NEW.unidades * NEW.precio;
      UPDATE productos p 
        SET p.cantidad = p.cantidad + (NEW.total - OLD.total) 
        WHERE p.producto = NEW.producto;
    END //
  DELIMITER ;

  -- Comprobamos su funcionamiento
  UPDATE ventas v 
    SET v.unidades = 120
    WHERE v.id = 8;

  SELECT * FROM ventas v;
  SELECT * FROM productos p;

